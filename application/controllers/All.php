<?php

class All extends CI_Controller {
    public function index()
    {
        $data['judul'] = 'Halaman All';
        $this->load->view('templates/header', $data);
        $this->load->view('all/index');
        $this->load->view('templates/footer');
    }
}