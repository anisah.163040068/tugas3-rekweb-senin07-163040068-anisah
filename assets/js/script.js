$(document).ready(function() {
	
	//ambil data json dari sumber
	$.getJSON('json/pizza.json', function(data) {

		//ambil data menu
		var menu = data.menu;

		//tampil data ke HTML
		$.each(menu, function(i,data){
			$('#daftar-menu').append($('<div class="col-sm-4"><div class="card"><img class="card-img-top" src="'+ data.gambar +'"><div class="card-body"><h4 class="card-title">'+ data.nama + '</h4><p class="card-text">'+ data.deskripsi +'</p><h3 class="card-title">'+ rupiah(data.harga) + '</h3><a href="#" class="btn btn-primary">Pesan Sekarang!</a></div></div>' ));
		});
	});	
});

//ketika link diklik
$('.nav-link').on('click', function(){
	var kategori = $(this).data('kategori');
	var judul = $(this).text();

	$('h1').html(judul);
	$('.nav-link').removeClass('active');
	$(this).addClass('active');

	$.getJSON('json/pizza.json', function(data) {
		//ambil data menu
		var menuAll = data.menu;

		//pilih menu berdasarkan kategori pada link yang di-link
		var menu = $.grep(menuAll, function(data){
			return data.kategori == kategori;
		});

		//jika kategori kosong / memilih link 'All menu'
		if (menu.length === 0) {
			menu = menuAll;
		}

		var hasil = '';
		$.each(menu, function(i, data){
			hasil += '<div class="col-sm-4"><div class="card"><img class="card-img-top" src="'+ data.gambar +'"><div class="card-body"><h4 class="card-title">'+ data.nama + '</h4><p class="card-text">'+ data.deskripsi +'</p><h3 class="card-title">'+ rupiah(data.harga) + '</h3><a href="#" class="btn btn-primary">Pesan Sekarang!</a></div></div>';
		});

		$('#daftar-menu').html(hasil);
	});

	//hentikan fungsi href
	return false;
})